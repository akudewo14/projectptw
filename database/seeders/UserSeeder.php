<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $mhs1 = new Mahasiswa;
        $mhs1->name = "user";
        $mhs1->role = "0";
        $mhs1->email = "user@gmail.com";
        $mhs1->password = "user1234";
        $mhs1->save();
        
        $mhs2 = new Mahasiswa;
        $mhs2->name = "admin";
        $mhs2->role = "1";
        $mhs2->email = "admin@gmail.com";
        $mhs2->password = "admin1234";
        $mhs2->save();

    }
}
