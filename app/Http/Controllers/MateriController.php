<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Materi;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role=Auth::user()->role;
        if($role=='1'){
            $data = Materi::All();
            return view('admin.tampil.materit' , ['Materi' => $data]);
        }else{
            $data = Materi::All();
            return view('student.tampil.materit' , ['Materi' => $data]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=Auth::user()->role;
        if($role=='1'){
            return view('admin.form.newmateri');
        }else{
            return abort(403,'Anda tidak punya akses karena anda bukan admin');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role=Auth::user()->role;
        if($role=='1'){
            $request->validate([
                'judul' => 'required',
                'isi' => 'required',
                'terms' => 'required'
            ]);
    
            DB::table('materi')->insert([
                'judul' => $request->judul,
                'isi' => $request->isi
            ]);
            // alihkan halaman ke halaman materi
            return redirect()->route('materi.index')->with('Success','Data Materi Berhasil Ditambah');
        }else{
            return abort(403,'Anda tidak punya akses karena anda bukan admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  Materi $materi
     * @return \Illuminate\Http\Response
     */
    public function show(Materi $materi)
    {
        return view('showin.materit', compact('materi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  Materi $materi
     * @return \Illuminate\Http\Response
     */
    public function edit(Materi $materi)
    {
        //
        $role=Auth::user()->role;
        if($role=='1'){
            return view('admin.edita.editmateri', compact('materi'));
        }else{
            return abort(403,'Anda tidak punya akses karena anda bukan admin');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Materi $materi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Materi $materi)
    {
        //
        $role=Auth::user()->role;
        if($role=='1'){
            $request->validate([
                'judul' => 'required',   
                'isi' => 'required',
                'terms' => 'required'
            ]);
    
            $materi->update([
                'judul' => $request->judul,
                'isi' => $request->isi
            ]);
    
            // alihkan halaman ke halaman materi
            return redirect()->route('materi.index')->with('Success','Data Dokter Berhasil Diubah');
        }else{
            return abort(403,'Anda tidak punya akses karena anda bukan admin');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Materi $materi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Materi $materi)
    {
        //
        $role=Auth::user()->role;
        if($role=='1'){
            $materi->delete();
            return redirect()->route('materi.index')->with('Success','Data Dokter Berhasil Dihapus');
        }else{
            return abort(403,'Anda tidak punya akses karena anda bukan admin');
        }
    }
}
